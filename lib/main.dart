import 'dart:ffi';

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: Scaffold(
        appBar: AppBar(title: Text("Calculator"),),
        body: CalcWidget(),
      ),
    );
  }
}

//***************************************************

class CalcWidget extends StatefulWidget{

  @override
  _CalcState createState() {
    return _CalcState();
  }

}

class _CalcState extends State<CalcWidget>{

  final num1Controller = TextEditingController();
  final num2Controller = TextEditingController();

  int _result = 0;
  String exception = "no exceptions yet";
  void _add(){
    setState(() {
      try{
        _result = int.parse(num1Controller.text) + int.parse(num2Controller.text);
      }
      catch(e){
        exception =" exception thrown: $e";
      }
    });
  }
  void _multiply(){
    setState(() {
    try{
      _result = int.parse(num1Controller.text) * int.parse(num2Controller.text);
    }
    catch(e){
      exception =" exception thrown: $e";
    }
    });
  }
  void _subtract(){
    setState(() {
      try{
        _result = int.parse(num1Controller.text) - int.parse(num2Controller.text);
      }
      catch(e){
        exception =" exception thrown: $e";
      }
    });
  }
  void _divide(){
    setState(() {
    try{
      _result = int.parse(num1Controller.text) ~/ int.parse(num2Controller.text);
    }
    catch(e){
      exception =" exception thrown: $e";
    }

    });
  }

  void _clear(){
    setState(() {
      num1Controller.text = "0";
      num2Controller.text = "0";
      _result = 0;
      exception ="no exceptions";
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TextField(controller: num1Controller,),
        TextField(controller: num2Controller ,),
        Text('$_result'),
        Text('exception: $exception'),
        FlatButton(
            onPressed: _add,
            color: Colors.blueAccent,
            child: Text("Add")),
        FlatButton(
            onPressed: _subtract,
            color: Colors.pink,
            child: Text("Subtract")),
        FlatButton(
            onPressed: _multiply,
            color: Colors.limeAccent,
            child: Text("Multiply")),
        FlatButton(
            onPressed: _divide,
            color: Colors.white60,
            child: Text("divide")),
        FlatButton(
            onPressed: _clear,
            color: Colors.redAccent,
            child: Text("Clear")),
      ],
    );
  }

}